﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using System;
using UnityEditor;


public class ColonManager : MonoBehaviour
    {
        
        public GameObject PrefabChild;
        public GameObject PrefabSoldier;
        public GameObject PrefabEngi;
        public GameObject PrefabFarmer;
        public GameObject PrefabMed;
        public GameObject PrefabScient;
        public GameObject PrefabWorker;
        public GameObject Clone;


        public static List<List<ColonClass.Human>> ColonList = new List<List<ColonClass.Human>>(8)
        {
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),
            new List<ColonClass.Human>(),

        };

        public void Manage(int rank, ColonClass.ColonType type)
        {
            if (type != ColonClass.ColonType.Soldier)
            {
                ColonList[rank].Add(new ColonClass.Human(type, 100, 100, 100, 100, rank, 100, 100, 0));
                ColonList[rank][ColonList[rank].Count - 1].SerialNumber = ColonList[rank].Count;
            }
            else
            {
                ColonList[rank].Add(new ColonClass.Human.Soldier(100, 1));
                if (ColonList[rank].Count == 1)
                    ColonList[rank][ColonList[rank].Count - 1].SerialNumber = 1;
                else
                {
                    ColonList[rank][ColonList[rank].Count - 1].SerialNumber = ColonList[rank][ColonList[rank].Count-2].SerialNumber+1;
                }
                
            }
        }
        // test
        public void RemoveColon(ColonClass.Human colon)
        {
            int i = 0;
            while (ColonList[colon.rank][i].SerialNumber != colon.SerialNumber)
                i++;
            ColonList[colon.rank].RemoveAt(i);
            string str = "";
            switch (colon.type)
            {
                case ColonClass.ColonType.Child:
                    str = "Child";
                    break;
                case ColonClass.ColonType.Engi:
                    str = "Engi";
                    break;
                case ColonClass.ColonType.Farmer:
                    str = "Farmer";
                    break;
                case ColonClass.ColonType.Med:
                    str = "Med";
                    break;
                case ColonClass.ColonType.Scient:
                    str = "Scient";
                    break;
                case ColonClass.ColonType.Soldier:
                    str = "Soldier";
                    break;
                case ColonClass.ColonType.Worker:
                    str = "Worker";
                    break;
            }
            Destroy(GameObject.Find(str+colon.SerialNumber));
        }

        public void CreateChild()
        {
            Manage(0,ColonClass.ColonType.Child);
            Clone = Instantiate(PrefabChild) as GameObject;
            Clone.transform.position = new Vector3(574,270,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Child" + (ColonList[0][ColonList[0].Count - 1].SerialNumber);
        }

        public void CreateSoldier()
        {
            Manage(1,ColonClass.ColonType.Soldier);
            Clone = Instantiate(PrefabSoldier) as GameObject;
            Clone.transform.position = new Vector3(574,270,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Soldier" + (ColonList[1][ColonList[1].Count - 1].SerialNumber);
        }

        public void CreateScient()
        {
            Manage(2,ColonClass.ColonType.Scient);
            Clone = Instantiate(PrefabScient) as GameObject;
            Clone.transform.position = new Vector3(604,270,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Scient" + (ColonList[2][ColonList[2].Count - 1].SerialNumber);
        }

        public void CreateWorker()
        {
            Manage(3,ColonClass.ColonType.Worker);
            Clone = Instantiate(PrefabWorker) as GameObject;
            Clone.transform.position = new Vector3(634,270,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Worker" + (ColonList[3][ColonList[3].Count - 1].SerialNumber);
        }

        public void CreateEngi()
        {
            Manage(4,ColonClass.ColonType.Engi);
            Clone = Instantiate(PrefabEngi) as GameObject;
            Clone.transform.position = new Vector3(664,270,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Engi" + (ColonList[4][ColonList[4].Count - 1].SerialNumber);
        }

        public void CreateFarmer()
        {
            Manage(5,ColonClass.ColonType.Farmer);
            Clone = Instantiate(PrefabFarmer) as GameObject;
            Clone.transform.position = new Vector3(604,240,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Farmer" + (ColonList[5][ColonList[5].Count - 1].SerialNumber);
        }

        public void CreateMed()
        {
            Manage(6,ColonClass.ColonType.Med);
            Clone = Instantiate(PrefabMed) as GameObject;
            Clone.transform.position = new Vector3(634,210,0);
            Clone.transform.localScale = new Vector3(100,100,0);
            Clone.name = "Med" + (ColonList[6][ColonList[6].Count - 1].SerialNumber);
        }

    }

