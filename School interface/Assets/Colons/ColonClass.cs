﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class ColonClass
{

    #region dependencies

    public enum Color
    {
        BLUE,
        RED,
        ORANGE,
        GREEN,
        GREY,
        WHITE,
        PURPLE
    }

    public enum ColonType
    {
        Med,
        Soldier,
        Engi,
        Farmer,
        Worker,
        Child,
        Scient
    }


    #endregion

    public class Human
    {
        public int SerialNumber;
        public int health;
        public int Food;
        public int Thirst;
        public int Rad;
        public int Moral;
        public int tiredness;
        public ColonType type;
        public int rank;

        #region Constructor;

        public Human(ColonType type, int Food, int Thirst, int Rad, int Moral, int rank, int tiredness, int health, int SerialNumber)
        {
            this.Food = Food;
            this.Thirst = Thirst;
            this.Rad = Rad;
            this.Moral = Moral;
            this.rank = rank;
            this.tiredness = tiredness;
            this.type = type;
            this.health = health;
            this.SerialNumber = SerialNumber;
        }

        #endregion

        public class Soldier : Human
        {
            public int Training;
            public int Strength;

            public Soldier(int training, int strength) : base(ColonType.Soldier, 100, 100, 100, 100, 1, 100, 100,0)
            {
                Training = training;
                Strength = strength;
            }
        }
    }

}