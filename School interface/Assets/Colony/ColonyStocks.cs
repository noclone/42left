﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mime;
using Unity.Collections.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class ColonyStocks : MonoBehaviour
{

    public static List<int> Ressource;
    public static List<(Buidling, bool)> BuildingList;
    void Start()
    {
        Ressource = new List<int>() {1110, 1110,11110, 1111, 11110, 1110,11110, 11110, 101111, 11110};
        BuildingList = new List<(Buidling, bool)>();
    }

    
    void Update()
    {
        
    }
    

    public string GetRessource(int x)
    {
        string str = "";
        switch (x)
        {
            case 0:
                str = "electricity";
                break;
            case 1:
                str = "food";
                break;
            case 2:
                str = "water";
                break;
            case 3:
                str = "oxygen";
                break;
            case 4:
                str = "wood";
                break;
            case 5:
                str = "plank";
                break;
            case 6:
                str = "rawOre";
                break;
            case 7:
                str = "metal";
                break;
            case 8:
                str = "skillPoints";
                break;
            case 9:
                str = "advanced item";
                break;
        }

        return str;
    }

    public enum RessouceName
    {
        Elec,
        Food,
        Water,
        Oxygen,
        Wood,
        Planks,
        RawOre,
        Metal,
        SkillPoints,
        AdvancedItem
    }
    
    public enum BuildingType
    {
        WaterPurifier,
        Kitchen,
        Dormitory,
        OxGen,
        HydrolicDam,
        Refinery,
        SawMill,
        Hospital,
        Barracks,
        Lab,
        Factory,
        Farm,
        School,
        CharCoalPowerPlant,
        WindMill,
        ExploCenter
    }

    public class Buidling
    {
        private BuildingType type;
        private int level;
        public int Consume1;
        public int Amount1;
        public int Consume2;
        public int Amount2;
        public int Produce;
        public int AmountProd;
        public int SerialNumber;

        public List<int> ToBuild = new List<int>() {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        public int Level
        {
            get => level;
        }

        public BuildingType Type
        {
            get => type;
        }

        public Buidling(BuildingType type, int SerialNumber)
        {
            this.type = type;
            level = 0;
            this.SerialNumber = SerialNumber;
            switch (type)
            {
                case BuildingType.Barracks:
                    Consume1 = 0;
                    Amount1 = 5000;
                    ToBuild[7] += 20;
                    ToBuild[5] += 400;
                    break;
                case BuildingType.Dormitory:
                    Consume1 = 0;
                    Amount1 = 1000;
                    ToBuild[5] += 200;
                    break;
                case BuildingType.Factory:
                    Consume1 = 0;
                    Amount1 = 80000;
                    Consume2 = 2;
                    Amount2 = 50000;
                    ToBuild[7] += 50;
                    ToBuild[5] += 200;
                    break;
                case BuildingType.Farm:
                    Consume1 = 0;
                    Amount1 = 1000;
                    Consume2 = 2;
                    Amount2 = 70000;
                    Produce = 1;
                    AmountProd = 20;
                    ToBuild[5] += 150;
                    break;
                case BuildingType.Hospital:
                    Consume1 = 0;
                    Amount1 = 30000;
                    ToBuild[7] += 30;
                    ToBuild[5] += 200;
                    break;
                case BuildingType.Kitchen:
                    Consume1 = 0;
                    Amount1 = 10000;
                    ToBuild[7] += 10;
                    ToBuild[5] += 100;
                    break;
                case BuildingType.Lab:
                    Consume1 = 0;
                    Amount1 = 20000;
                    Consume2 = 2;
                    Amount2 = 50000;
                    ToBuild[7] += 30;
                    ToBuild[5] += 100;
                    ToBuild[9] += 5;
                    break;
                case BuildingType.Refinery:
                    Consume1 = 0;
                    Amount1 = 100000;
                    ToBuild[7] += 50;
                    ToBuild[5] += 50;
                    break;
                case BuildingType.School:
                    Consume1 = 0;
                    Amount1 = 1000;
                    ToBuild[7] += 5;
                    ToBuild[5] += 400;
                    break;
                case BuildingType.HydrolicDam:
                    Produce = 0;
                    AmountProd = 300000;
                    ToBuild[7] += 10;
                    ToBuild[5] += 500;
                    break;
                case BuildingType.OxGen:
                    Consume1 = 0;
                    Amount1 = 30000;
                    Produce = 3;
                    AmountProd = 300000;
                    ToBuild[7] += 30;
                    ToBuild[5] += 20;
                    break;
                case BuildingType.SawMill:
                    Consume1 = 0;
                    Amount1 = 40000;
                    ToBuild[7] += 20;
                    ToBuild[5] += 80;
                    break;
                case BuildingType.WaterPurifier:
                    Consume1 = 0;
                    Amount1 = 40000;
                    Produce = 2;
                    AmountProd = 240000;
                    ToBuild[7] += 30;
                    ToBuild[5] += 20;
                    break;
                case BuildingType.WindMill:
                    Produce = 0;
                    AmountProd = 100000;
                    ToBuild[7] += 15;
                    ToBuild[9] += 5;
                    break;
                case BuildingType.CharCoalPowerPlant:
                    Produce = 0;
                    AmountProd = 400000;
                    Consume1 = 4;
                    Amount1 = 1;
                    ToBuild[7] += 30;
                    ToBuild[5] += 200;
                    ToBuild[9] += 10;
                    break;
                case BuildingType.ExploCenter:
                    Consume1 = 0;
                    Amount1 = 5000;
                    ToBuild[7] += 70;
                    ToBuild[5] += 100;
                    ToBuild[9] += 10;
                    break;
            }
        }

        public void UpgradeBuilding()
        {
            level++;
        }
    }
}

