﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using Polybrush;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;


public class placmentscript : MonoBehaviour
{
    
    public enum CursorState{Building,Rotating}
 
    public CursorState state = CursorState.Building;
    public Transform Pointeur;
    public Transform Objectplaced;
    public GameObject[] BuildingToPlace;
    public LayerMask mask;
    GameObject builtObject;
    float LastPosX,LastPosY,LastPosZ;
    Vector3 mousePos;
    structure Structure;


    public static int numberofthebuilding =-1;
    

    public static float convert(float value, float multipleof)
    {
        return (float) Math.Round((decimal) value / (decimal) multipleof, MidpointRounding.ToEven) *
               multipleof;
    }


    // Update is called once per frame
    void Update()
    {
        float rotate;
        mousePos = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;


        if (EventSystem.current.IsPointerOverGameObject())
            return;
        
        if (numberofthebuilding==-1)
            Pointeur.position = Vector3.zero; //place le pointeur en 0,0,0(dans la montagne sous le sol)
        
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask) && numberofthebuilding!=-1)
        {
            int PosX = (int) Mathf.Round(convert(hit.point.x,5));
            int PosZ = (int) Mathf.Round(convert(hit.point.z,5));
            
            
            if ((PosX != LastPosX ||  PosZ != LastPosZ )&& !Input.GetMouseButton(0) ) //prend les coordonnées de la sourie
            {
                LastPosX = PosX;
                LastPosZ = PosZ;
                Pointeur.position = new Vector3(PosX, 12.5f, PosZ);
            }

            if(Input.GetMouseButton(0))
            {
                if(state == CursorState.Building)
                {
                    Objectplaced.position=new Vector3(convert(PosX,5),12.5f,convert(PosZ,5)); //place le bâtiment dans les positions indiquées
                    GameObject building = Instantiate(BuildingToPlace[numberofthebuilding],Objectplaced.position,Quaternion.identity); //place le bâtiment
                    Structure = building.GetComponent<structure>();

                    builtObject = building;
                    state = CursorState.Rotating;
                }

                if (state == CursorState.Rotating)
                {
                    rotate = 0f;                     /////
                    if (Input.GetKeyDown(KeyCode.A)) //
                        rotate += 90;                // pour pivoter lors en mode rotate
                    if (Input.GetKeyDown(KeyCode.E)) //
                        rotate -= 90;                /////
                    builtObject.transform.Rotate(0,rotate,0);
                   
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                bool IsEnough = true;
                int n = 0;
                
                ColonyStocks.BuildingType type = ColonyStocks.BuildingType.OxGen; // obligé d'init la variable (le type OxGen n'a pas d'importance)
                
                switch (numberofthebuilding)
                {
                    case 1:
                        type = ColonyStocks.BuildingType.School;
                        break;
                    case 2:
                        type = ColonyStocks.BuildingType.WaterPurifier;
                        break;
                    case 3:
                        type = ColonyStocks.BuildingType.Kitchen;
                        break;
                    case 4:
                        type = ColonyStocks.BuildingType.Refinery;
                        break;
                }
                
                ColonyStocks.Buidling newBuild = new ColonyStocks.Buidling(type,GetNumberOfBuildingBuilt(type));
                
                foreach (var VARIABLE in newBuild.ToBuild)
                {
                    if (VARIABLE > ColonyStocks.Ressource[n])
                        IsEnough = false;
                    n++;
                }
                if (!Structure.okay || !IsEnough)
                {
                    Destroy(builtObject); //Si mal placé alors est détruit
                }
                else
                {
                    n = 0;
                    foreach (var VARIABLE in newBuild.ToBuild)
                    {
                        ColonyStocks.Ressource[n] -= VARIABLE;
                        n++;
                    }

                    ColonyStocks.BuildingList.Add((newBuild, true));

                    numberofthebuilding = -1; //après avoir placé le bâtiment, ne met plus rien dans la main

                    GameObject CloneInterface = builtObject;
                    
                   if (type != ColonyStocks.BuildingType.School)
                   {
                       string AddToInterface = "People Inside : ";
                        switch (type)
                        {
                            case ColonyStocks.BuildingType.WaterPurifier:
                                builtObject.name = "WaterPurifier";
                                break;
                            case ColonyStocks.BuildingType.Barracks:
                                AddToInterface += Barracks.PeopleInside;
                                builtObject.name = "Barracks";
                                builtObject.AddComponent<Barracks>();
                                break;
                            case ColonyStocks.BuildingType.Dormitory:
                                AddToInterface += Dormitory.PeopleInside;
                                builtObject.name = "Dormitory";
                                builtObject.AddComponent<Dormitory>();
                                break;
                            case ColonyStocks.BuildingType.Factory:
                                AddToInterface += Factory.PeopleInside;
                                builtObject.name = "Factory";
                                builtObject.AddComponent<Factory>();
                                break;
                            case ColonyStocks.BuildingType.Farm:
                                AddToInterface += Farm.PeopleInside;
                                builtObject.name = "Farm";
                                builtObject.AddComponent<Farm>();
                                break;
                            case ColonyStocks.BuildingType.Hospital:
                                AddToInterface += Hospital.PeopleInside + "\nAvailable Medics : " +
                                                 Hospital.AvailableMed + "\nOccupied Medics :" + Hospital.OccupiedMed;
                                builtObject.name = "Hospital";
                                builtObject.AddComponent<Hospital>();
                                break;
                            case ColonyStocks.BuildingType.Kitchen:
                                builtObject.name = "Kitchen";
                                AddToInterface += global::Kitchen.PeopleInside;
                                builtObject.AddComponent<Kitchen>();
                                break;
                            case ColonyStocks.BuildingType.Lab:
                                AddToInterface += lab.PeopleInside;
                                builtObject.name = "Lab";
                                builtObject.AddComponent<lab>();
                                break;
                            case ColonyStocks.BuildingType.Refinery:
                                AddToInterface += global::Refinery.PeopleInside;
                                builtObject.name = "Refinery";
                                builtObject.AddComponent<Refinery>();
                                break;
                            case ColonyStocks.BuildingType.ExploCenter:
                                builtObject.name = "ExploCenter";
                                AddToInterface = "Scientists Inside : " + ExploCenter.ScientInside +
                                                 "Soldiers Inside : " + ExploCenter.SoldierInside;
                                builtObject.AddComponent<ExploCenter>();
                                break;
                            case ColonyStocks.BuildingType.SawMill:
                                builtObject.name = "SawMill";
                                AddToInterface += Sawmill.PeopleInside;
                                builtObject.AddComponent<Sawmill>();
                                break;

                        }

                        builtObject.name += GetNumberOfBuildingBuilt(type);
                        
                        CloneInterface = Instantiate(GameObject.Find("Default Interface"),
                            builtObject.transform);
                        CloneInterface.name = builtObject.name + "Interface";
                        CloneInterface.tag = "Interface";


                        Text text = builtObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>();
                        
                        text.text += builtObject.name+"\n\n";
                        if (AddToInterface != "People Inside : ")
                        {
                            text.text += AddToInterface+"\n\n";
                        }
                        if (newBuild.Amount1 != 0)
                        {
                            text.text += "Consume :\n\n" + newBuild.Amount1 + " " + GetRessource(newBuild.Consume1) +
                                        " / day";
                        }

                        if (newBuild.Amount2 != 0)
                        {
                            text.text += "\n" + newBuild.Amount2 + " " + GetRessource(newBuild.Consume2) + " / day";
                        }

                        if (newBuild.AmountProd != 0)
                        {
                            text.text += "\n\n" + "Produce :" + "\n\n" + newBuild.AmountProd + " " +
                                         GetRessource(newBuild.Produce) + " / day";
                        }
                    }
                }

                builtObject = null;
                state = CursorState.Building;

            }
        }
    }

    public int GetNumberOfBuildingBuilt(ColonyStocks.BuildingType type)
    {
        int n = 0;
        foreach (var VARIABLE in ColonyStocks.BuildingList)
        {
            if (VARIABLE.Item1.Type == type)
                n++;
        }
        return n;
    }

/// ---BâtimentsIndex--- \\\
/// En dessous mettre les numéros des bâtiments en cas de rajout et les placer dans le script dans la caméra
    public void Bunker()
    {
        numberofthebuilding = 0;
    }

    public void School()
    {
        numberofthebuilding = 1;
    }

    public void WaterPurifier()
    {
        numberofthebuilding = 2;
    }

    public void Kitchen()
    {
        numberofthebuilding = 3;
    }

    public void Refinery()
    {
        numberofthebuilding = 4;
    }
    
    public string GetRessource(int x)
    {
        string str = "";
        switch (x)
        {
            case 0:
                str = "electricity";
                break;
            case 1:
                str = "food";
                break;
            case 2:
                str = "water";
                break;
            case 3:
                str = "oxygen";
                break;
            case 4:
                str = "wood";
                break;
            case 5:
                str = "plank";
                break;
            case 6:
                str = "rawOre";
                break;
            case 7:
                str = "metal";
                break;
            case 8:
                str = "skillPoints";
                break;
            case 9:
                str = "advanced item";
                break;
        }

        return str;
    }
}
