﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickOnObject : MonoBehaviour
{

    private string name;
    void Start()
    {
    }

    void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                name = hit.collider.name;
                if (hit.collider.tag=="Building")
                {
                    CloseAllInterfaces();
                    if (name.Contains("school"))
                    {
                        GameObject.Find("folder").transform.position = new Vector3(500,300,0);
                    }
                    else
                    {
                        GameObject.Find(name).transform.GetChild(0).gameObject.SetActive(true);
                        GameObject.Find(name).transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseAllInterfaces();
        }

    }

    public void CloseAllInterfaces()
    {
        foreach (var VARIABLE in GameObject.FindGameObjectsWithTag("Interface"))
        {
            VARIABLE.SetActive(false);
        }

        GameObject.Find("folder").transform.position = new Vector3(999, 999, 999);
    }
}
