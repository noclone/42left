﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using UnityEngine;
public class ExploCenter : MonoBehaviour
{
    public static int ScientInside;
    public static int SoldierInside;
    private List<ColonClass.Human> IndoorScient = new List<ColonClass.Human>();
    private List<ColonClass.Human> IndoorSoldier = new List<ColonClass.Human>();
    private List<ColonClass.Human> Crew = new List<ColonClass.Human>();

    void Start()
    {
        
    }
    
    void Update()
    {
        if (ScientInside == 1 && SoldierInside == 2)
        {
            Crew.Add(IndoorScient[0]);
            IndoorScient.RemoveAt(0);
            Crew.Add(IndoorSoldier[0]);
            Crew.Add(IndoorSoldier[1]);
            IndoorSoldier.RemoveAt(1);
            IndoorSoldier.RemoveAt(0);
            LaunchExplo();
            ScientInside--;
            SoldierInside -= 2;
            for (int i = 2; i >= 0; i--)
            {
                ColonOut(Crew[i]);
                Crew.RemoveAt(i);
            }
        }
    }

    public void ColonEnter(ColonClass.Human colon)
    {
        switch (colon.type)
        {
            case ColonClass.ColonType.Scient:
                ScientInside++;
                IndoorScient.Add(colon);
                break;
            case ColonClass.ColonType.Soldier:
                IndoorSoldier.Add(colon);
                SoldierInside++;
                break;
        }
    }

    public void ColonOut(ColonClass.Human colon)
    {
        
    }

    public async void LaunchExplo()
    {
        await Task.Delay(600000);
        if (Random.Range(1, 3) == 2)
        {
            ColonyStocks.Ressource[8]++;
        }
    }

}
