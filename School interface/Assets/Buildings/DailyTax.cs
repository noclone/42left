﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DailyTax : MonoBehaviour
{
    public bool InGame = true;
    // Start is called before the first frame update
    async void Start()
    {
        while (InGame)
        {
            await Task.Delay(10000);
            Taxes();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Taxes()
    {
        foreach (var element in ColonyStocks.BuildingList)
        {
            if(element.Item2)
                Consume(element.Item1);
        }
    }

    public void Consume(ColonyStocks.Buidling building)
    {
        ColonyStocks.Ressource[building.Consume1] -= building.Amount1*building.Level;
        ColonyStocks.Ressource[building.Consume2] -= building.Amount2*building.Level;
        ColonyStocks.Ressource[building.Produce] += building.AmountProd*building.Level;
    }
}
