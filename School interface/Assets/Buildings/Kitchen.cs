﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.EventSystems;

public class Kitchen : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public async void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        await Task.Delay(10000);
        colon.Food = 100;
        colon.Thirst = 100;
        ColonyStocks.Ressource[2] -= 10;
        ColonyStocks.Ressource[1] -= 1;
        ColonOut(colon);
    }

    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
    
}
