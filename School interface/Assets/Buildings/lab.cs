﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;

public class lab : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    private int prob;
    private int Timer = 300000;
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    
    public void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        Search();
        ColonOut(colon);
    }

    public async void Search()
    {
        await Task.Delay(Timer -= PeopleInside*15);
        prob = Random.Range(1, 4);
        if (prob == 2)
        {
            ColonyStocks.Ressource[8] += 1;
        }
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
}
