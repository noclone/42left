﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.UI;

public class Refinery : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();

    private GameObject chargingBar;
    private GameObject backgroundBar;

    private float time;

    private bool IsOnWork;
    private int test;
    void Start()
    {
        backgroundBar = Instantiate(GameObject.Find("Image (2)"));
        backgroundBar.transform.SetParent(GameObject.Find("Canvas").transform);
        chargingBar = Instantiate(GameObject.Find("fill2"));
        chargingBar.transform.SetParent(GameObject.Find("Canvas").transform);
        chargingBar.transform.position = backgroundBar.transform.position;
        while (time < 3)
        {
            time += Time.deltaTime;
            chargingBar.GetComponent<Image>().fillAmount = time / 3;
        }
        
    }
    
    void Update()
    {
        if (IsOnWork)
        {
            
        }
        else
        {
            
        }
    }
    
    public void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        Produce();
        ColonOut(colon);
    }

    public async void Produce()
    {
        IsOnWork = true;
        await Task.Delay(10000/PeopleInside);
        ColonyStocks.Ressource[7] += 2;
        ColonyStocks.Ressource[6] -= 1;
        IsOnWork = false;

    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
    public int GetNumberOfBuildingBuilt(ColonyStocks.BuildingType type)
    {
        int n = 0;
        foreach (var VARIABLE in ColonyStocks.BuildingList)
        {
            if (VARIABLE.Item1.Type == type)
                n++;
        }
        return n;
    }
}
