﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Dormitory : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public async void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        await Task.Delay(20000);
        colon.tiredness = 100;
        ColonOut(colon);
    }

    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
        //get colon out to be coded
    }
}
