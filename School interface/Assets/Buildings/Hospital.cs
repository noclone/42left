﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Hospital : MonoBehaviour
{
    public static int OccupiedMed;
    public static int AvailableMed;
    public static int PeopleInside;
    private List<ColonClass.Human> WaitingList = new List<ColonClass.Human>();
    private List<ColonClass.Human> MedList = new List<ColonClass.Human>();
    void Start()
    {
        
    }
    
    void Update()
    {
        if (AvailableMed > 0 && WaitingList.Count > 0)
        {
            AvailableMed--;
            OccupiedMed++;
            TakeCare(WaitingList[WaitingList.Count-1]);
            WaitingList.RemoveAt(WaitingList.Count-1);
        }
        
    }

    public async void TakeCare(ColonClass.Human colon)
    {
        await Task.Delay(60000); 
        colon.health = 100; 
        ColonOut(colon); 
        AvailableMed++; 
        OccupiedMed--;
    }
    
    public void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        WaitingList.Add(colon);
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        //get colon out to be coded
    }

    public void MedEnter(ColonClass.Human Med)
    {
        AvailableMed++;
        MedList.Add(Med);
    }

    public void MedOut(ColonClass.Human Med)
    {
        AvailableMed--;
        MedList.Remove(Med);
    }
}