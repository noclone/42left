﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Factory : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public async void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        if (ColonyStocks.Ressource[7] >= 5)
        {
            await Task.Delay(40000);
            ColonyStocks.Ressource[7] -= 5;
            ColonyStocks.Ressource[9] += 1;  
        }
        ColonOut(colon);
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
}
