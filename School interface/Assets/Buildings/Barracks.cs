﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Barracks : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
    }

    void Update()
    {
        
    }
    
    public async void ColonEnter(ColonClass.Human.Soldier colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        await Task.Delay(30000);
        colon.Training = 100;
        colon.Strength++;
        ColonOut(colon);
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
}
