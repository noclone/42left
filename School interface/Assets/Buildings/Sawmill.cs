﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Sawmill : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public async void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
        await Task.Delay(5000);
        ColonyStocks.Ressource[5] += 10;
        ColonyStocks.Ressource[4] -= 1;
        ColonOut(colon);
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }    
}