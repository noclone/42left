﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Farm : MonoBehaviour
{
    public static int PeopleInside;
    private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public void ColonEnter(ColonClass.Human colon)
    {
        PeopleInside++;
        IndoorPop.Add(colon);
    }
    
    public void ColonOut(ColonClass.Human colon)
    {
        PeopleInside--;
        IndoorPop.Remove(colon);
    }
}